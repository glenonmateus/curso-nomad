server = true
data_dir = "/opt/consul"

bootstrap_expect = 1
client_addr = "0.0.0.0"
advertise_addr = "172.35.1.100"
retry_join = ["172.35.1.100"]

ports {
  grpc = 8502
}

connect {
  enabled = true
}

