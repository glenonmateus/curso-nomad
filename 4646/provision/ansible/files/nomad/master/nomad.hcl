log_level = "INFO"
datacenter = "dc1"
data_dir = "/opt/nomad"
bind_addr = "0.0.0.0"
name = "nomad-master"

#acl {
#  enabled = true
#}

#advertise {
#  http = "{{GetInterfaceIP \"eth1\"}}"
#  rpc  = "{{GetInterfaceIP \"eth1\"}}"
#  serf = "{{GetInterfaceIP \"eth1\"}}"
#}

# Só estou colocando aqui um outro exemplo:
advertise {
  http = "172.35.1.100"
  rpc  = "172.35.1.100"
  serf = "172.35.1.100"
}

consul {
  address             = "127.0.0.1:8500"
  server_service_name = "nomad"
  client_service_name = "nomad-master"
  auto_advertise      = true
  server_auto_join    = true
  client_auto_join    = true
}

server {
  enabled = true
  bootstrap_expect = 1
}
