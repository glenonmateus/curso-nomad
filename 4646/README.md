Laboratório 4646 - Nomad: Utilização do Nomad como orquestrador de containeres
=============================

Repositório para armazenamento dos materiais necessários do curso introdutório sobre o Hashicorp Nomad: Utilização do Nomad como orquestrador de containeres.

Requisitos iniciais:
------------

Para a criação do laboratório é necessário ter instalados os seguintes softwares:

* [Git][2]
* [VirtualBox][3]
* [Vagrant][4]

> Para o máquinas com Windows aconselho, se possível, que as instalações sejam feitas pelo gerenciador de pacotes **[Cygwin][5]**.

> Para as máquinas com MAC OS aconselho, se possível, que as instalações sejam feitas pelo gerenciador de pacotes **brew**.

Ambiente para prática
-----------

O ambiente para prática será criado utilizando o [Vagrant][6]. Ferramenta para criação e gerenciamento ambientes virtualizados (baseado em Inúmeros providers) com foco em automação.

Nesse laboratório, que está centralizado no arquivo [Vagrantfile][7], será criada 1 maquina com a seguinte característica:

Nome       | vCPUs | Memoria RAM | IP            | S.O.¹
---------- |:-----:|:-----------:|:-------------:|:---------------:
nomad-master     | 1     | 1024MB | 172.35.1.100 | ubuntu-18.04-amd64
nomad-node1      | 1     | 512MB  | 172.35.1.101 | ubuntu-18.04-amd64
nomad-node2      | 1     | 512MB  | 172.35.1.102 | ubuntu-18.04-amd64
nomad-registry   | 1     | 512MB  | 172.35.1.103 | ubuntu-18.04-amd64

> **¹**: Esses Sistemas operacionais estão sendo utilizados no formato de Boxes, é a forma como o vagrant denomina as imagens do sistema operacional utilizado.

Criação do Laboratório
----------------------

Para criar o laboratório é necessário fazer o `git clone` desse repositório e, dentro da pasta baixada realizar a execução do `vagrant up`, conforme abaixo:

```bash
git clone https://github.com/eduardosramos/curso-nomad
cd curso-nomad/4646
vagrant up
```

_O Laboratório **pode demorar**, dependendo da conexão de internet e poder computacional, para ficar totalmente preparado._
> Em caso de erro na criação das máquinas sempre valide se sua conexão está boa, os logs de erros na tela e, se necessário, o arquivo **/var/log/vagrant_provision.log** dentro da máquina que apresentou a falha.

Por fim, para melhor utilização, abaixo há alguns comandos básicos do vagrant para gerencia das máquinas virtuais.

Comandos                | Descrição
:----------------------:| ---------------------------------------
`vagrant init`          | Gera o VagrantFile
`vagrant box add <box>` | Baixar imagem do sistema
`vagrant box status`    | Verificar o status dos boxes criados
`vagrant up`            | Cria/Liga as VMs baseado no VagrantFile
`vagrant provision`     | Provisiona mudanças logicas nas VMs
`vagrant status`        | Verifica se VM estão ativas ou não.
`vagrant ssh <vm>`      | Acessa a VM
`vagrant ssh <vm> -c <comando>` | Executa comando via ssh
`vagrant reload <vm>`   | Reinicia a VM
`vagrant halt`          | Desliga as VMs

> Para maiores informações acesse a [Documentação do Vagrant][8]

[2]: https://git-scm.com/downloads
[3]: https://www.virtualbox.org/wiki/Downloads
[4]: https://www.vagrantup.com/downloads
[5]: https://cygwin.com/install.html
[6]: https://www.vagrantup.com/
[7]: ./Vagrantfile
[8]: https://www.vagrantup.com/docs
